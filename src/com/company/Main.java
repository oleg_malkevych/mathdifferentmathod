package com.company;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Введіть n : ");
        int n = input.nextInt();

        double a = -1;

        double b = 1;

        double[] zValueArrayWhereNEqual1 = {0};
        double[] cValueArrayWhereNEqual1 = {2};


        double[] zValueArrayWhereNEqual2 = {- 0.577350 , 0.577350};
        double[] cValueArrayWhereNEqual2 = {1 , 1};

        double[] zValueArrayWhereNEqual3 = { - 0.774597 , 0 , 0.774597};
        double[] cValueArrayWhereNEqual3 = { 0.555555 , 0.888888 , 0.555555};

        double[] zValueArrayWhereNEqual4 = { - 0.861136 , - 0.339981 , 0.339981 , 0.861136};
        double[] cValueArrayWhereNEqual4 = { 0.347855 , 0.652145 , 0.652145 , 0.347855};

        double[] zValueArrayWhereNEqual5 = { - 0.906180 , - 0.538470 , 0 , 0.538470 , 0.906180};
        double[] cValueArrayWhereNEqual5 = { 0.236927 , 0.478629 , 0.568888 , 0.478629 , 0.236927};

        double[] zValueArrayWhereNEqual6 = { - 0.932470 , - 0.661210 , - 0.238620 , 0.238620 , 0.661210 , 0.932470};
        double[] cValueArrayWhereNEqual6 = { 0.171324 ,  0.360761 , 0.467914 , 0.467914 , 0.360761 , 0.171324};

        double[] zValueArrayWhereNEqual7 = { - 0.949108 , - 0.741531 , - 0.405845 , 0 , 0.405845 , 0.741531 , 0.949108};
        double[] cValueArrayWhereNEqual7 = { 0.129485 , 0.279705 , 0.381830 , 0.417960 , 0.381830 , 0.279705 , 0.129485};

        double[] zValueArrayWhereNEqual8 = {- 0.960290 , - 0.796666 , - 0.525532 , - 0.183434 , 0.183434 , 0.525532 , 0.796666 , 0.960290};
        double[] cValueArrayWhereNEqual8 = { 0.101228 , 0.222381 , 0.313707 , 0.362684 , 0.362684 , 0.313707 , 0.222381 , 0.101228};

        double[] zValueArrayWhereNEqual9 = { - 0.968160 , - 0.836031 , - 0.613371 , - 0.324253 , 0 , 0.324253 , 0.613371 , 0.836031 , 0.968160 };
        double[] cValueArrayWhereNEqual9 = { 0.911589 , 0.601019 , 0.528762 , 0.167906 , 0 , 0.167906 , 0.528762 , 0.601019 , 0.911589 };

        double[] zValueArrayWhereNEqual10 = { - 0.973906 , - 0.865063 , - 0.679409 , - 0.433395 , - 0.148874 , 0.148874 , 0.433395 , 0.679409 , 0.865063 , 0.973906 };
        double[] cValueArrayWhereNEqual10 = { 0.066671 , 0.149451 , 0.219086 , 0.167906 , 0.269266 , 0.295524 , 0.295524 , 0.269266 , 0.167906 , 0.219086 , 0.149451 , 0.066671 };

        double [] zArray = new double[n];
        double sumOfRow = 0;
        double answerToFunction;

        if (n == 1){
            for (int i = 0; i < zValueArrayWhereNEqual1.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual1[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual1.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual1[i] * (zArray[i] * Math.sqrt(Math.pow((8 - Math.pow(zArray[i] , 2)) , 3))); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }
        if (n == 2){
            for (int i = 0; i < zValueArrayWhereNEqual2.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual2[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual2.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual2[i] * (zArray[i] / (zArray[i] + 1)); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }

        if (n == 3){
            for (int i = 0; i < zValueArrayWhereNEqual3.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual3[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual3.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual3[i] * (zArray[i] * Math.sqrt(Math.pow((8 - Math.pow(zArray[i] , 2)) , 3))); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }

        if (n == 4){
            for (int i = 0; i < zValueArrayWhereNEqual4.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual4[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual4.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual4[i] * (zArray[i] / (3 * zArray[i] - 4)); //міняти на функцію //IMPORTANT
                System.out.print((b - a) / 2 * (cValueArrayWhereNEqual4[i] * (zArray[i] / (3 * zArray[i] - 4))) + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }

        if (n == 5){
            for (int i = 0; i < zValueArrayWhereNEqual5.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual5[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
//                System.out.println("z" + (i + 1) + zValueArrayWhereNEqual5[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual5.length ; i++) {
                sumOfRow += (cValueArrayWhereNEqual5[i] * (zArray[i] / (3 * zArray[i] - 4))); //міняти на функцію //IMPORTANT
                System.out.print((b - a) / 2 * (cValueArrayWhereNEqual5[i] * (zArray[i] / (3 * zArray[i] - 4))) + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }

        if (n == 6){
            for (int i = 0; i < zValueArrayWhereNEqual6.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual6[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual6.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual6[i] * (zArray[i] * Math.sqrt(Math.pow((8 - Math.pow(zArray[i] , 2)) , 3))); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }

        if (n == 7){
            for (int i = 0; i < zValueArrayWhereNEqual7.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual7[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual7.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual7[i] * (zArray[i] * Math.sqrt(Math.pow((8 - Math.pow(zArray[i] , 2)) , 3))); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }
        if (n == 8){
            for (int i = 0; i < zValueArrayWhereNEqual8.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2  * zValueArrayWhereNEqual8[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual8.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual8[i] * (zArray[i] * Math.sqrt(Math.pow((8 - Math.pow(zArray[i] , 2)) , 3))); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }
        if (n == 9){
            for (int i = 0; i < zValueArrayWhereNEqual9.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual9[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual9.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual9[i] * (zArray[i] * Math.sqrt(Math.pow((8 - Math.pow(zArray[i] , 2)) , 3))); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }
        if (n == 10){
            for (int i = 0; i < zValueArrayWhereNEqual10.length; i++) {
                zArray[i] = (b + a) / 2 + (b - a) / 2 * zValueArrayWhereNEqual10[i];
                System.out.println("z" + (i + 1) + " = " + zArray[i]);
            }
            System.out.print("Answer = ");
            for (int i = 0; i < zValueArrayWhereNEqual10.length ; i++) {
                sumOfRow += cValueArrayWhereNEqual10[i] * (zArray[i] * Math.sqrt(Math.pow((8 - Math.pow(zArray[i] , 2)) , 3))); //міняти на функцію //IMPORTANT
                System.out.print(sumOfRow + " + ");
            }
            answerToFunction = (b - a) / 2 * sumOfRow;
            System.out.print("= " + answerToFunction);
        }

    }
}
